import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {useState, useEffect, useContext} from 'react'
import './App.css';
import { Container } from "react-bootstrap";
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Products from './pages/Products';
import SpecificProduct from './pages/SpecificProduct';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Cart from './pages/Cart';
import SuccessOrder from './pages/SuccessOrder';
import Error from './pages/Error';
import {UserProvider} from './UserContext';


function App() {

  const [user, setUser] = useState({
    // Temporarily Store it to localStorage
    // Email: localStorage.getItem("Email")
    // if working comment out the local Storage and set for the user id and isAdmin
    id: null,
    isAdmin: null

  });
  console.log(user);
  // use useEffect function so that the loggin user can retain its page everytime it's reload/refresh
  /* Since reloading the app resets our user state's properties to null, we need to re-retrieve the id and isAdmin values from our API
    
    TO do so, we run a useEffect hook with a fetch request then re-set the user state's properties
  */
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/user-details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {

        // set our user state to include the user's id and isAdmin values
        if(typeof data._id !== 'undefined'){
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        } else {
          setUser({
            id: null,
            isAdmin: null
          })
        }
    })
  }, [])


  return (

    <UserProvider value={{user,setUser}}>
      <Router>
        <>
          <AppNavBar />

          <Container>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/products" component={Products} />
                <Route exact path="/products/:productId" component={SpecificProduct} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/logout" component={Logout} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/cart" component={Cart} />
                <Route exact path="/success-order" component={SuccessOrder} />

              {/*Redirect to Error page if invalid URL*/}
              <Route component={Error} />

            </Switch>
          </Container>

        </>
      </Router>
    </UserProvider>
  );
}

export default App;
