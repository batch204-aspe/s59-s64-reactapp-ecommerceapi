const productData = [
	{
		id: "product101",
		Name: "Lenovo Legion 5",
		Description: "Lenovo Legion 5 AMD Ryzen 7-5800H 16GB 512GB SSD RTX 3060 Win11 Phantom Blue Laptop PS",
		Price: 72000,
		isActive: true
	},

	{
		id: "product102",
		Name: "Asus TUF Gaming F15 FX506HC-HN083W",
		Description: "Asus  TUF Gaming F15 FX506HC-HN083W   Intel Core I5-11400H 8GB DDR4 512GB M.2 NVMe PCIe3.0 SSD RTX3050 4GD6 Win11 Laptop  1 Year Warranty Budget Laptop Same Day Delivery Brand New",
		Price: 45000,
		isActive: tru
	},

	{
		id: "product103",
		Name: "Asus FA507RC-HN022W",
		Description: "Asus FA507RC-HN022W 15.6inch FHD Ryzen 7-6800H 8GB 512G SSD RTX3050 Windows 11 Laptop Mecha RED",
		Price: 59500,
		isActive: true
	}	
]

export default productData;