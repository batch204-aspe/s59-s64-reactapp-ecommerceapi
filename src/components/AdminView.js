import { useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';

export default function AdminView(props){
	// console.log(props);

	//destructure the productProp and the fetchData function from Product.js
	const { productsProp, fetchData } = props;

	const [productsArr, setProductsArr] = useState([])
	const [productId, setProductId] = useState("")
	
	const [Name, setName] = useState("")
	const [Description, setDescription] = useState("")
	const [Price, setPrice] = useState(0)

	//states for handling modal visibility
	const [showEdit, setShowEdit] = useState(false)
	const [showAdd, setShowAdd] = useState(false)

	const [isActive, setIsActive] = useState(false);

	const token = localStorage.getItem("token")

	//Functions to handle opening and closing modals
	const openEdit = (productId) => {
		//console.log(productId)
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			// console.log(data);
			setProductId(data._id);
			setName(data.Name);
			setDescription(data.Description);
			setPrice(data.Price);
		});

		setShowEdit(true)
	}

	const closeEdit = () => {
			setProductId("");
			setName("");
			setDescription("");
			setPrice(0);

		setShowEdit(false)
	}

	// edit Product
	const editProduct = (e) => {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {

			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				Name: Name,
				Description: Description,
				Price: Price

			})
		})
		.then(res => res.json())
		.then(data => {
			if (data) {
				alert("Product Successfully Updated.");
				// Close the modal and set all states back to default values
				closeEdit();
				// we call fetchData here to update the data we receive from the database
				// callingfetchData updates our coursesProp, which the useEffect below is monitoring
				// since the courseProp updates, the useEffect runs again, which re-renders our table with the updated data
				fetchData();

			} else {
				alert("Something went wrong.");
			}
		});
	}

	//Functions to handle opening and closing modals
	const openAdd = () => {
		
		setName("");
		setDescription("");
		setPrice();
	
		setShowAdd(true)
	}

	const closeAdd = () => {

			setProductId("");
			setName("");
			setDescription("");
			setPrice(0);

		setShowAdd(false)
	}

	// Add New Product
	const addProduct = (e) => {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/add-product`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				Name: Name,
				Description: Description,
				Price: Price
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data) {
				alert("Product Successfully Added.");
				closeAdd();
				fetchData();
			} else {
				alert("Something went wrong.");
			}
		});

	}

	const archiveToggle = (productId, isActive) => {
		// console.log(productId, isActive);
		fetch(`${process.env.REACT_APP_API_URL}/products/archive/${productId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				isActive: !isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			if(data) {
				let bool

				isActive ? bool = "disabled" : bool = "enabled"

				alert (`Product Successfully ${bool}`)
				fetchData()
			}else {
				alert("Something went wrong");
			}
		})

	}

	useEffect(() => {
		//map through the productsProp to generate table contents
		const products = productsProp.map(product => {
			return(
				<tr key={product._id}>
					<td>{product.Name}</td>
					<td>{product.Description}</td>
					<td>{product.Price}</td>
					<td>
							{/*Dynamically render product availability*/}
							{product.isActive
								? <span>Available</span>
								: <span>Unavailable</span>
							}
					</td>
					<td>
						<Button variant="primary" size="sm" onClick={() => openEdit(product._id)}>Update</Button>
						{product.isActive
							//dynamically render which button show depending on product availability
							? <Button variant="danger" size="sm"  onClick={() => archiveToggle(product._id, product.isActive)}>Disable</Button>
							: <Button variant="success" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Enable</Button>
						}
					</td>
				</tr>

			)
		})

		//set the CoursesArr state with the results of our mapping so that it can be used in the return statement
		setProductsArr(products)

	}, [productsProp]);
	
	return(
		<>
			<h2 className="text-center mt-5 mb-3">Admin Dashboard</h2>
			<Button className="mb-4" variant="primary" size="lg" onClick={() => openAdd()}>Add Product</Button>


			{/*Product info table*/}
			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{/*Mapped table contents dynamically generated from the coursesProp*/}
					{productsArr}
				</tbody>
			</Table>


			{/*Edit Product Modal*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Update Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group controlId="editProductName">
							<Form.Label>Name</Form.Label>
							<Form.Control
								value={Name}
								onChange={e => setName(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="editProductDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control
								value={Description}
								onChange={e => setDescription(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="editProductPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control
								value={Price}
								onChange={e => setPrice(e.target.value)}
								type="number"
								required
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>


			{/*Add Product Modal*/}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group controlId="addProductName">
							<Form.Label>Name</Form.Label>
							<Form.Control
								value={Name}
								onChange={e => setName(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="addProductDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control
								value={Description}
								onChange={e => setDescription(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="addProductPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control
								value={Price}
								onChange={e => setPrice(e.target.value)}
								type="number"
								required
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

		</>
	)
}
