import { useContext } from 'react';
import {Container, Nav, Navbar, NavDropdown} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';
import UserContext from '../UserContext';


export default function AppNavBar () {

	// A context object such as our UserContext can be "opened" with React's useContext hook
	const { user } = useContext(UserContext);

	return (
		<Navbar bg="dark" variant="dark" expand="lg">
		    <Container>
		        <Navbar.Brand as={Link} to="/"> MK DigiShop </Navbar.Brand>

		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="ms-auto">

		            <Nav.Link as={NavLink} to="/"> Home </Nav.Link>
		            <Nav.Link as ={NavLink} to="/products">Product</Nav.Link>
		            
		            {	
		            	(user.id !== null) ?
		            	<>
		            		{ (user.isAdmin) ?
		            			<Nav.Link as={NavLink} to="/logout"> Logout </Nav.Link>
		            			:
		            			<>
		            				<Nav.Link as={NavLink} to="/cart"> Cart </Nav.Link>
		            				<Nav.Link as={NavLink} to="/logout"> Logout </Nav.Link>
		            			</>
		            		}
		            		
		            	</>
		            	:
		            	<>
		            		<Nav.Link as={NavLink} to="/login"> Login </Nav.Link>
		            		<Nav.Link as={NavLink} to="/register"> Register </Nav.Link>
		            	</>
		            	
		            }

		          </Nav>
		        </Navbar.Collapse>
		    </Container>
		</Navbar>
	);
}