import { useEffect, useState, useContext } from 'react';
import ProductCard from '../components/ProductCard';
import AdminView from "../components/AdminView";
import UserContext from '../UserContext';


export default function Products () {

	const [productsData, setProductsData] = useState([]);

	const {user} = useContext(UserContext);
	console.log(user)

	const fetchData = () => {

		fetch(`${process.env.REACT_APP_API_URL}/products/all-product`)
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setProductsData(data);
		});
	};

	// Get Request
    useEffect(() => {

		fetchData();

	}, []);

	const products = productsData.map(product => {

		if(product.isActive){

			return(

				<ProductCard productProp={product} key={product._id} />
			);

		} else {

			return null
		}

	})

	return (
		(user.isAdmin) ?
			<AdminView productsProp={productsData} fetchData={fetchData} />
		:
		<>
			<h1 className="mt-5 mb-4 text-center">Products</h1>
			{products}
		</>
	);
}