import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';
import UserContext from '../UserContext';


export default function Register(props) {

	// Declaring the Input Type STates of Email, Password & isActive Button State
	// SYntax: const [getter, setter] = useState(initialValue);
	// States - Input Type 
	const [Email, setEmail] = useState("");
	const [Password, setPassword] = useState("");
	const [VerifyPassword, setVerifyPassword] = useState("");
	// STates - Button Submit
	const [isActive, setIsActive] = useState(false);

	// useContext is a another type of Hook like userState & useEffect
	// store the userContext in a State
	const { user, setUser } = useContext(UserContext);
	// after declaring the useContext State bind it to a function

	// Bind the STates to the useEffect Hook
	// capture the input .value on the event using onChange
	// The target of the onChange event is the input .value 
	useEffect(() => {

		if( (Email !== '' && Password !== '' && VerifyPassword !=='') && (Password === VerifyPassword) ) { // Check if the input are populated & Password is match to verify password
			setIsActive(true);

		} else {

			setIsActive(false);
		}

	}, [Email, Password, VerifyPassword]);

	

	// Create a Function that BINDS the useEffect Hook
	function registerUser(e){
		e.preventDefault(); // prevent default behaviour, so that the form does not submit

		// For Temporary Store the login to localStorage to see if its geting the data
		// localStorage.setItem allows us to save a key/value pair to localStorage
		// Syntax: localStorage.setItem('key', value);
		/*localStorage.setItem('Email', Email);*/

		// after setting the localStorage
		// Store it to the useContext setter state
		/*setUser({
			Email: Email
		});*/

		// After Checking the localStorage is working
		// now connect it to the Database
		// using the fetch() method linked the DB using the DB endpoints or routes
		// User Register Routes
		// 1st Check if the user email is existing
		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				Email: Email
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data){ // if the EMail is exist

				alert("Email is Already Exists");

			} else { // if the user's email is not existing proceed to register endpoint
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {

					method: "POST",
					headers:{
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({

						Email: Email,
						Password: Password

					})
				})
				.then(res => res.json())
				.then(data => {
					if(data){ // if Successfully Registered
						alert("Succesfully Registered.");

						props.history.push("login");

					} else { // not successful
						alert("Please try again. Something went wrong");
					}
				})
			}

		})

		

		// setEmail("");
		// setPassword("");
		// setVerifyPassword("");
		// alert(`Thank you for registering ${Email}`);
	}


	return (
		(user.id !== null) ?
			<Redirect to="/" />
		:
		// bind the function authenticateUser to Form using onSumBit event
		// then add the onChange event to Form.Control
		<Form className="form-width mt-5 p-3" onSubmit={e => registerUser(e)}>

			<h2 className="text-center">Register</h2>

			<Form.Group className="mt-3" controlId="userEmail">
				<Form.Label>Email:</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter your email"
					value={Email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group className="mt-3" controlId="password">
				<Form.Label>Password:</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your password"
					value={Password}
					onChange={e => setPassword(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group className="mt-3" controlId="verifyPassword">
				<Form.Label>Verify Password:</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify your password"
					value={VerifyPassword}
					onChange={e => setVerifyPassword(e.target.value)}
					required
				/>
			</Form.Group>

			{/* Validate Submit button if all fields are populated if not make it disabled */}
			{ isActive ?
				<Button className="mt-3" variant="primary" type="submit" id="submitBtn">
					Submit
				</Button>
				:
				<Button className="mt-3" variant="primary"  id="submitBtn" disabled>
					Submit
				</Button>
			}

			<p className="text-right mt-3">
			          Already have an account yet? <Link to="/login">Click here</Link> to Login.
			</p>

		</Form>

	);
}